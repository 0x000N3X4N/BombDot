#pragma once
#include <iostream>
#include "SFML/Graphics.hpp"
#include "BoardFunc.hpp"

class Board {
	size_t state[5][5]{0};
	bool board[5][5]{ 0 }; 
	bool lose = false;
public:
	void DrawBoard();
	void putMines();
	void openTileNew(int x, int y);
	void GameOver(sf::RenderWindow &app, sf::Texture& texture);
	int _cdecl rand_();
};