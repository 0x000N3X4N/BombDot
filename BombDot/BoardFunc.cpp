#include "BoardFunc.hpp"

void Board::DrawBoard() { // recognize new window
	sf::RenderWindow app(sf::VideoMode(611, 520), "BombDot");
	sf::Texture texture;
	texture.loadFromFile("texture_now.png");
	sf::Sprite sprite(texture);
	sf::Event event;
	while (app.isOpen()) {
		sf::Vector2i pixelPos = sf::Mouse::getPosition(app);
		sf::Vector2f pos = app.mapPixelToCoords(pixelPos);
		(lose) ? GameOver(app, texture) : 0;
		while (app.pollEvent(event)) {
			(event.type == sf::Event::Closed) ? app.close() : 0;
			(event.type == sf::Event::MouseButtonReleased && !lose) ? openTileNew(static_cast<int>(pos.x / 119), static_cast<int>(pos.y / 100)) : 0;
		}
		if (!lose)
			for (size_t x = 0; x < 5; x++)
			{
				for (size_t y = 0; y < 5; y++)
				{
					if (state[x][y] == 1) { //bomb 
						sprite.setTextureRect(sf::IntRect(117, 0, 119, 101));
						lose = true;
					}
					else if (state[x][y] == 2) //miss
						sprite.setTextureRect(sf::IntRect(250, 0, 119, 101));
					else
						sprite.setTextureRect(sf::IntRect(0, 0, 119, 101));

					sprite.setPosition(static_cast<float>(x * 123), static_cast<float>(y * 105));
					app.draw(sprite);
				}
			}
		app.display();
	}
}

void Board::GameOver(sf::RenderWindow& app, sf::Texture& texture) {
	sf::Image gameOverBackground;
	gameOverBackground.loadFromFile("GameOver_pic.png");
	gameOverBackground.createMaskFromColor(sf::Color::Black);

	sf::RectangleShape RectGameOverBackground;
	texture.loadFromImage(gameOverBackground);
	RectGameOverBackground.setTexture(&texture);
	RectGameOverBackground.setSize(sf::Vector2f(app.getSize()));
	app.draw(RectGameOverBackground);
}

int  _cdecl Board::rand_() {
	int num(0);
	__asm {
	Tut:
		xor edx,edx
		jmp Main 
	Main:
		xor edx, edx
		rdrand eax
		mov ebx, 7
		idiv ebx
		cmp edx, 5
		jge Tut
		mov [num], edx
	}
	return num;
}
void Board::putMines() {
	for (size_t i = 0; i < 15; i++)
		board[rand_()][rand_()] = 1;
}

void Board::openTileNew(int x, int y) {
	if (board[x][y] != 0) state[x][y] = 1; else state[x][y] = 2; /*if we find a bomb state = 1, and if we missed state = 2*/
}
